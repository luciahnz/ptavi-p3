#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.producto = []

    def startElement(self, name, attrs):

        if name == 'root-layout':
            self.width = attrs.get('width', "")
            self.height = attrs.get('height', "")
            self.background_color = attrs.get('background-color', "")
            self.root_layout = {"etiqueta": "root-layout",
                                'width': self.width, 'height': self.height,
                                'background-color': self.background_color}
            self.producto.append(self.root_layout)

        elif name == 'region':
            self.id = attrs.get('id', "")
            self.top = attrs.get('top', "")
            self.bottom = attrs.get('bottom', "")
            self.left = attrs.get('left', "")
            self.rigth = attrs.get('rigth', "")
            self.region = {"etiqueta": "region", 'id': self.id,
                           'top': self.top, 'bottom': self.bottom,
                           'left': self.left, 'rigth': self.rigth}
            self.producto.append(self.region)

        elif name == 'img':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            self.img = {"etiqueta": "img ", 'src': self.src,
                        'region': self.region, 'begin': self.begin,
                        'dur': self.dur}
            self.producto.append(self.img)

        elif name == 'audio':
            self.src = attrs.get('src', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            self.audio = {"etiqueta": "audio", 'src': self.src,
                          'begin': self.begin, 'dur': self.dur}
            self.producto.append(self.audio)

        elif name == 'textstream':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.textstream = {"etiqueta": "textstream",
                               'src': self.src, 'region': self.region}
            self.producto.append(self.textstream)

    def get_tags(self):
        return (self.producto)


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
