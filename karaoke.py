import sys
import json
import urllib.request
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler


class KaraokeLocal():
    def __init__(self, fich):
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        self.producto = cHandler.get_tags()
        parser.parse(open(sys.argv[1]))

    def __str__(self):
        linea = ''
        for line in self.producto:
            for name in line:
                if line[name] != "":
                    linea += name + ' = "' + line[name] + '"' + "\t"
            linea += "\n"
        return (linea)

    def to_json(self, fich):
        json.dump(self.producto, open('karaoke.json', "w"), indent=4)

    def do_local(self, fich):
        for line in self.producto:
            for name in line:
                if "http" in line[name]:
                    urllib.request.urlretrieve(line[name],
                                               line[name].split("/")[-1])


if __name__ == "__main__":

    try:
        fich = sys.argv[1]
    except IndexError:
        sys.exit('Usage: python3 karaoke.py file.smil.')

    karaoke = KaraokeLocal(fich)
    print(karaoke)
    karaoke.to_json(fich)
    karaoke.do_local(fich)
    karaoke.to_json("local.json")
    print(karaoke)
